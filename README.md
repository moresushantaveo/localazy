# lazytest

A new Flutter project.

## Steps to follow for Localization

- To setup localizations, firstly go to https://localazy.com/ and sign up for an account.
- Once account is created, then you can proceed to create a project


## Usage

- Go to https://localazy.com/, Login and go to dashboard to add any required keys and translations.
- Then with the Help of Localazy CLI, run command '''localazy download''' to get newly built .arb files with new translations.
- Once new arb files are downloaded, run command '''flutter gen-l10n''' to generate new localizations from arb files.
- You can use the localization string as Followed : 

'''dart
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

Text(AppLocalizations.of(context)!.<YOUR_TRANSLATION_KEY>),
'''