import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  Map<String, String> locales = {
    'en': "English",
    'gu': "Gujrati",
    'hi': "Hindi",
  };
  RxString selectedLocale = ''.obs;
  List<DropdownMenuItem<String>> items = <DropdownMenuItem<String>>[].obs;

  @override
  void onInit() {
    locales.forEach((key, value) {
      items.add(DropdownMenuItem(
        value: key,
        child: Text(value),
      ));
    });
    selectedLocale.value = items[0].value!;
    super.onInit();
  }

  void changeLocale(String locale) {
    selectedLocale.value = locale;
    Get.updateLocale(Locale(locale));
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
