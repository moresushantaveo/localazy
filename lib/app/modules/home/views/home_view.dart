import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:get/get.dart';
import 'package:lazytest/app/modules/home/widgets/text_fields.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sign Up'),
        centerTitle: true,
      ),
      body: Center(
        child: Card(
            elevation: 18,
            child: Container(
              width: 400,
              padding: const EdgeInsets.all(28),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Obx(
                    () => Align(
                      alignment: Alignment.centerRight,
                      child: DropdownButton(
                        value: controller.selectedLocale.value,
                        items: controller.items,
                        onChanged: (value) {
                          controller.changeLocale(value.toString());
                        },
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 18.0,
                  ),
                  Text(AppLocalizations.of(context)!.firstName),
                  const FieldText(),
                  const SizedBox(
                    height: 18.0,
                  ),
                  Text(AppLocalizations.of(context)!.lastName),
                  const FieldText(),
                  const SizedBox(
                    height: 18.0,
                  ),
                  Text(AppLocalizations.of(context)!.email),
                  const FieldText(),
                  const SizedBox(
                    height: 18.0,
                  ),
                  Text(AppLocalizations.of(context)!.password),
                  const FieldText(),
                  const SizedBox(
                    height: 24.0,
                  ),
                  ElevatedButton(
                    onPressed: () {
                      print(AppLocalizations.supportedLocales.toList());
                      // Get.updateLocale(AppLocalizations.supportedLocales.);
                    },
                    style: ButtonStyle(
                        shape: MaterialStateProperty.all(RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(24)))),
                    child: Container(
                      width: double.infinity,
                      padding: const EdgeInsets.all(16),
                      child: Text(
                        AppLocalizations.of(context)!.submit,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                ],
              ),
            )),
      ),
    );
  }
}
