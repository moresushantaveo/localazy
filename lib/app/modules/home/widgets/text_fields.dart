import 'package:flutter/material.dart';

class FieldText extends StatelessWidget {
  const FieldText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: TextFormField(
          decoration:
              InputDecoration(fillColor: Colors.grey[100], filled: true)),
    );
  }
}
